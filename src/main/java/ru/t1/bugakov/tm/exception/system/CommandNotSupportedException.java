package ru.t1.bugakov.tm.exception.system;

public final class CommandNotSupportedException extends AbstractSystemException {

    public CommandNotSupportedException() {
        super("Error! This command is not supported.");
    }

    public CommandNotSupportedException(final String command) {
        super("Error! This command - \"" + command + "\" is not supported.");
    }
}
