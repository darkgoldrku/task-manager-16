package ru.t1.bugakov.tm.api.ProjectTask;

public interface IProjectTaskController {

    void bindTaskToProject();

    void unbindTaskToProject();

}
