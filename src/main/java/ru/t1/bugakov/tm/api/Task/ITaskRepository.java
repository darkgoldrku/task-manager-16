package ru.t1.bugakov.tm.api.Task;

import ru.t1.bugakov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    void add(Task task);

    List<Task> findAll();

    List<Task> findAll(Comparator comparator);

    List<Task> findAllByProjectId(String projectId);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    void clear();

    void remove(Task task);

    Task removeById(String id);

    Task removeByIndex(Integer index);

}
