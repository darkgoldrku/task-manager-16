package ru.t1.bugakov.tm.api.Comparator;

import ru.t1.bugakov.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(final Status status);

}
