package ru.t1.bugakov.tm.api.ProjectTask;

public interface IProjectTaskService {

    void bindTaskToProject(String projectId, String taskId);

    void unbindTaskToProject(String projectId, String taskId);

    void removeProjectById(String projectId);

}
