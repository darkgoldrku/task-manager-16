package ru.t1.bugakov.tm.service;

import ru.t1.bugakov.tm.api.Command.ICommandRepository;
import ru.t1.bugakov.tm.api.Command.ICommandService;
import ru.t1.bugakov.tm.model.Command;

public final class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
