package ru.t1.bugakov.tm.service;

import ru.t1.bugakov.tm.api.Project.IProjectRepository;
import ru.t1.bugakov.tm.api.ProjectTask.IProjectTaskService;
import ru.t1.bugakov.tm.api.Task.ITaskRepository;
import ru.t1.bugakov.tm.exception.field.*;
import ru.t1.bugakov.tm.exception.entity.*;
import ru.t1.bugakov.tm.model.Task;

import java.util.List;

public final class ProjectTaskService implements IProjectTaskService {

    private final IProjectRepository projectRepository;

    private final ITaskRepository taskRepository;

    public ProjectTaskService(final IProjectRepository projectRepository, final ITaskRepository taskRepository) {
        this.projectRepository = projectRepository;
        this.taskRepository = taskRepository;
    }

    @Override
    public void bindTaskToProject(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        final Task task = taskRepository.findOneById(taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(projectId);
    }

    @Override
    public void unbindTaskToProject(final String projectId, final String taskId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (taskId == null || taskId.isEmpty()) throw new TaskIdEmptyException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        final Task task = taskRepository.findOneById(taskId);
        if (task == null) throw new TaskNotFoundException();
        task.setProjectId(null);
    }

    @Override
    public void removeProjectById(final String projectId) {
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        if (!projectRepository.existsById(projectId)) throw new ProjectNotFoundException();
        final List<Task> tasks = taskRepository.findAllByProjectId(projectId);
        for (final Task task : tasks) taskRepository.remove(task);
        projectRepository.removeById(projectId);
    }

}
