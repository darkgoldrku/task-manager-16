# TASK MANAGER

## DEVELOPER INFO

**NAME:** Danil Bugakov

**EMAIL:** dbugakov@t1-consulting.ru

**EMAIL:** darkgoldrku@gmail.com

## SOFTWARE

**EMAIL:** OPENJDK 1.8

**OS:** WINDOWS 10 EDUCATION

## HARDWARE

**CPU:** Intel Xeon E5-2660v3

**RAM:** 32GB

**SSD:** 512GB

## RUN PROGRAM

```
java -jar ./task-manager.jar
```